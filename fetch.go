// SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"fmt"
	"os"
    "time"
	"log"
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/mmcdole/gofeed"
	"gopkg.in/ini.v1"
)

type Feed struct {
    gorm.Model
    Name string `ini:"name"`
    Feed string `ini:"feed"`
    Link string `ini:"link"`
    Location string `ini:"location"`
    Avatar string  `ini:"avatar"`
    Author string `ini:"author"`
    Post []Post `gorm:"foreignkey:FeedRefer"`
}

type Post struct {
    gorm.Model
    Url string
    Title string
    Content string
    Published *time.Time
    FeedRefer uint
}

func main() {

    os.Remove("planet.db")

    db, err := gorm.Open("sqlite3", "planet.db")
    if err != nil {
      panic("failed to connect database")
    }
    defer db.Close()

    // Migrate the schema
    db.AutoMigrate(&Feed{})
    db.AutoMigrate(&Post{})

    contributor_id := 1

	cfg, err := ini.Load("planet.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

    fmt.Printf("Read file")

	fp := gofeed.NewParser()
    lenght := len(cfg.Sections())
	for index, contributor := range cfg.Sections() {
        feed := new(Feed)
        err := contributor.MapTo(feed)
		if err != nil { 
			fmt.Printf("Error parsing %v(feed): %v", contributor, err)
			continue
		}
        db.Create(&feed)
        
        feedP, err := fp.ParseURL(feed.Feed)
		if err != nil {
			fmt.Printf("%v\n", err)
            contributor_id += 1
			continue
		}
        for _, item := range feedP.Items {
            db.Model(&feed).Association("Post").Append(Post{Url: item.Link, Title: item.Title, Content: item.Content, Published: item.PublishedParsed});
		}

        contributor_id += 1

        log.Printf("%d / %d \n", index, lenght)
	}

    /*
    rows, err := db.Query("SELECT * FROM entry")
    if err != nil {
        log.Fatal(err)
    }
    defer rows.Close()

    for rows.Next() {
        var (
            id int64
            url string
            title string
            content string
            published string
            feed int64
        )
        if err := rows.Scan(&id, &url, &title, &content, &published, &feed); err != nil {
            log.Fatal(err)
        }
    }
    */
}